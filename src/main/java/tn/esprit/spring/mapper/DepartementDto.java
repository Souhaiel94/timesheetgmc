package tn.esprit.spring.mapper;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;



@Getter
@Setter
public class DepartementDto {

	@JsonProperty("id")
    private int id;

  
    @JsonProperty("name")
    private String name;


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public DepartementDto(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}


	public DepartementDto(){}

    

}
